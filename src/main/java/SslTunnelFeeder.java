import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class SslTunnelFeeder extends Thread {
    private InputStream in;
    private OutputStream out;
    private String name;

    public SslTunnelFeeder(InputStream in, OutputStream out, String name) {
        this.in = in;
        this.out = out;
        this.name = name;
    }

    @Override
    public void run() {
        System.out.println(name + " (" + getId() + ")" + " feeder started feeding encrypted data");
        try {
            IOUtils.copy(in, out);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            System.out.println(name + " (" + getId() + ")" + " feeder finished");
        }
    }
}
