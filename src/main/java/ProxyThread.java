import rawhttp.core.RawHttp;
import rawhttp.core.RawHttpRequest;
import rawhttp.core.RawHttpResponse;

import java.net.*;
import java.io.*;

public class ProxyThread extends Thread {
    private Socket socket;
    private Socket sslTunnel;
    public ProxyThread(Socket socket) {
        super("ProxyThread");
        this.socket = socket;
    }

    public void run() {
        try {
            RawHttp rawHttp = new RawHttp();
            RawHttpRequest request = rawHttp.parseRequest(readHttpMessageFromClient());
            System.out.println("Request from client: \n" + request.toString());

            if(request.getMethod().equals("CONNECT") && request.getUri().getPort() == 443) {
                sslTunnel = new Socket(request.getUri().getHost(), request.getUri().getPort() == -1 ? 80 : request.getUri().getPort());

                if(sslTunnel.isConnected()) {
                    RawHttpResponse<?> okResponse = rawHttp.parseResponse("HTTP/1.1 200 OK");
                    okResponse.writeTo(socket.getOutputStream());
                    SslTunnelFeeder feeder1 = new SslTunnelFeeder(socket.getInputStream(), sslTunnel.getOutputStream(), "CLIENT -> SERVER");
                    SslTunnelFeeder feeder2 = new SslTunnelFeeder(sslTunnel.getInputStream(), socket.getOutputStream(), "SERVER -> CLIENT");
                    feeder1.start();
                    feeder2.start();
                    feeder1.join();
                    feeder2.join();
                }
            } else {
                Socket backendSocket = new Socket(request.getUri().getHost(), request.getUri().getPort() == -1 ? 80 : request.getUri().getPort());
                request.writeTo(backendSocket.getOutputStream());

                RawHttpResponse<?> response = rawHttp.parseResponse(backendSocket.getInputStream());
                System.out.println("Response from server: \n" + response.toString());

                response.writeTo(socket.getOutputStream());
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                    if(sslTunnel != null) {
                        sslTunnel.close();
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String readHttpMessageFromClient() throws IOException {
        BufferedReader in = new BufferedReader(
                new InputStreamReader(socket.getInputStream()));

        String inputLine;
        StringBuilder clientRequest = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            clientRequest.append(inputLine);
            if(inputLine.isEmpty()) {
                break;
            }
            clientRequest.append("\n");
        }

        return clientRequest.toString();
    }
}
